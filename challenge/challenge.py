"""Flask app that contains a train and predict endpoint."""
from flask import Flask, request, make_response
import io
import pandas as pd
import numpy as np
from sklearn.linear_model import LogisticRegression
from sklearn.model_selection import train_test_split
import spacy
import os
import pickle


app = Flask(__name__)


@app.route('/genres/train', methods=['POST'])
def train():
    """Training endpoint.

    This method is called on a post request to the specified URL.
    The post request should comply with the specified API

    Returns
    -------
    message: str
        static response message
    """
    raw_data = request.get_data()
    bytes_io = io.BytesIO(raw_data)
    df = pd.read_csv(bytes_io)
    model = MovieModel()
    model.train(df)
    model.save()
    return 'The model was trained successfully'


@app.route('/genres/predict', methods=['POST'])
def predict():
    """Prediction endpoint.

    This method is called on a post request to the specified URL.
    The post request should comply with the specified API.

    Returns
    -------
    response: flask.response
        response containing CSV file of predictions,
        compliant with the specified API.
    """
    raw_data = request.get_data()
    bytes_io = io.BytesIO(raw_data)
    df_input = pd.read_csv(bytes_io)
    model = MovieModel()
    model.load()
    df = model.predict(df_input)
    string_io = io.StringIO()
    df.to_csv(string_io, index=False)
    response = make_response(string_io.getvalue())
    response.headers['Content-Type'] = 'text/csv'
    return response


class MovieModel:
    """Model that predicts movie genres based on its synopsis.

    Attributes
    ----------
    spacy_dim: int
        dimension of Spacy word vectors
    folder: str
        folder where model is stored
    labels_file: str
        name of the file where labels are stored
    nlp: spacy.lang.en.English)
        Spacy language model
    binary_models: List[BinaryModel]
        list of binary classification models for each genre
    labels: List[str]
        list of all genres the model detects
    n_lables: int
        number of labels
    """

    spacy_dim = 300
    folder = 'movie_model'
    labels_file = 'labels.pickle'

    def __init__(self):
        """Model constructor which initializes language model."""
        try:
            self.nlp = spacy.load("en_core_web_lg")
        except OSError:
            os.system('python -m spacy download en_core_web_lg')
            self.nlp = spacy.load("en_core_web_lg")
        self.binary_models = None
        self.labels = None
        self.n_labels = 0

    def load(self):
        """Load a trained model from the hard drive."""
        path = os.path.join(self.folder, self.labels_file)
        with open(path, 'rb') as f:
            self.labels = pickle.load(f)
        self.binary_models = []
        self.n_labels = len(self.labels)
        for i in range(self.n_labels):
            label = self.labels[i]
            model = BinaryModel(label, i)
            model.load(self.folder)
            self.binary_models.append(model)

    def save(self):
        """Save a trained model to the hard drive."""
        if not os.path.isdir(self.folder):
            os.mkdir(self.folder)
        path = os.path.join(self.folder, self.labels_file)
        with open(path, 'wb') as f:
            pickle.dump(self.labels, f)
        for model in self.binary_models:
            model.save(self.folder)

    def train(self, df):
        """Train the model for the given data.

        Parameters
        ----------
        df: pandas.DataFrame
            dataframe containing 'movie_id', 'synopsis' and 'genres' columns
        """
        print('Preprocess dataframe')
        df = self.preprocess(df)
        n_samples = df.shape[0]
        self.labels = list(df.columns[2:])
        self.n_labels = len(self.labels)
        print('Calculate document vectors')
        x = np.zeros((n_samples, self.spacy_dim))
        for i in range(n_samples):
            x[i, :] = self.doc_to_vector(df.loc[i, 'synopsis'])
        y = df.iloc[:, 2:].values
        print('Fit ensemble models')
        self.binary_models = []
        for i in range(self.n_labels):
            label = self.labels[i]
            model = BinaryModel(label, i)
            model.train(x, y[:, i])
            self.binary_models.append(model)

    def predict(self, df_input):
        """Train the output for the given data.

        Parameters
        ----------
        df_input: pandas.DataFrame
            dataframe containing 'movie_id', 'synopsis'

        Returns
        -------
        df: pandas.DataFrame
            input dataframe appended with column 'predicted_genres'
        """
        df = df_input[['movie_id']].copy()
        n_samples = df.shape[0]
        x = np.zeros((n_samples, self.spacy_dim))
        for i in range(n_samples):
            x[i, :] = self.doc_to_vector(df_input.loc[i, 'synopsis'])
        p = np.zeros((n_samples, self.n_labels))
        for i in range(self.n_labels):
            model = self.binary_models[i]
            p[:, i] = model.predict_proba(x)
        top5 = np.flip(np.argsort(p, axis=1), axis=1)[:, 0:5]
        df_top5 = pd.DataFrame(top5)
        for i in df_top5.columns:
            df_top5[i] = df_top5[i].apply(lambda x: self.labels[x])
        df['predicted_genres'] = df_top5.apply(
            lambda x: ' '.join(list(x)),
            axis=1
        )
        return df

    @staticmethod
    def preprocess(df):
        """Preprocess training dataframe.

        Parameters
        ----------
        df: pandas.DataFrame
            dataframe containing 'movie_id', 'synopsis' and 'genres' columns

        Returns
        -------
        df: pandas.DataFrame
            dataframe with labels als 0/1 columns
        """
        df = df[['movie_id', 'synopsis', 'genres']]
        for i in df.index:
            genres = df.loc[i, 'genres'].split(' ')
            for genre in genres:
                if not (genre in df.columns):
                    df[genre] = 0
                df.loc[i, genre] = 1
        del df['genres']
        return df

    def token_filter(self, token):
        """Check whether token is used in document vectorization.

        Parameters
        ----------
        token: spacy.Token
            word in sentence

        Returns
        -------
        flag: bool
            True if accepted, False otherwise
        """
        if token.lemma_ in self.nlp.Defaults.stop_words:
            return False
        if token.pos_ == "PUNCT":
            return False
        return True

    def doc_to_vector(self, text):
        """Convert text string to vector.

        Parameters
        ----------
        text: str
            raw text

        Returns
        -------
        vector: np.array
            doc vector, shape=(300,)
        """
        doc = self.nlp(text)
        v = np.zeros(self.spacy_dim)
        count = 0
        for token in doc:
            if self.token_filter(token):
                v += token.vector
                count += 1
        if count > 0:
            v /= count
        return v


class BinaryModel:
    """Binary classification model for one movie genre.

    Attributes
    ----------
    n_resample: int
        number of balanced resamples of training set
    label: str
        movie genre name
    label_index: int
        movie genre index
    weak_learners: List[obj]
        list of classification models
    """

    n_resample = 20

    def __init__(self, label, label_index):
        """Binary model constructor.

        Parameters
        ----------
        label: str
            movie genre name
        label_index: int
            movie genre index
        """
        self.label = label
        self.label_index = label_index
        self.weak_learners = None

    def load(self, folder):
        """Load from hard drive.

        Parameters
        ----------
        folder: str
            root folder name
        """
        folder = os.path.join(folder, self.label)
        file_names = [x for x in os.listdir(folder) if x.endswith('.pickle')]
        file_names.sort()
        self.weak_learners = []
        for file_name in file_names:
            with open(os.path.join(folder, file_name), 'rb') as f:
                self.weak_learners.append(pickle.load(f))

    def save(self, folder):
        """Save to hard drive.

        Parameters
        ----------
        folder: str
            root folder name
        """
        folder = os.path.join(folder, self.label)
        if not os.path.isdir(folder):
            os.mkdir(folder)
        count = 0
        for weak_learner in self.weak_learners:
            file_name = f'model_{count}.pickle'
            with open(os.path.join(folder, file_name), 'wb') as f:
                pickle.dump(weak_learner, f)
            count += 1

    def train(self, x, y):
        """Train the binary model.

        Parameters
        ----------
        x: np.array
            input training data, shape=(n_samples, spacy_dim)
        y: np.array
            binary output training data, shape=(n_samples)
        """
        self.weak_learners = []
        for i in range(self.n_resample):
            x_train, y_train = self.sample_balanced(x, y, i * self.label_index)
            model = LogisticRegression(solver='newton-cg')
            model.fit(x_train, y_train)
            self.weak_learners.append(model)

    def predict_proba(self, x):
        """Predict succes probabilities.

        Parameters
        ----------
        x: np.array
            input data, shape=(n_samples, spacy_dim)

        Returns
        -------
        p: np.array
            probabilities, shape=(n_samples)
        """
        p = np.zeros(x.shape[0])
        for model in self.weak_learners:
            index_1 = np.where(model.classes_ == 1)[0][0]
            p += model.predict_proba(x)[:, index_1]
        p /= len(self.weak_learners)
        return p

    def sample_balanced(self, x, y, random_state):
        """Sample from training set such that resulting set is balanced.

        Parameters
        ----------
        x: np.array
            input training data, shape=(n_in, 300)
        y: np.array
            output training data, shape=(n_in)

        Returns
        -------
        x_train: np.array
            sampled input training data, shape=(n_out, 300)
        y_train: np.array
            sampled output training data, shape=(n_out)
        """
        idx0 = np.where(y == 0)[0]
        n0 = len(idx0)
        idx1 = np.where(y == 1)[0]
        n1 = len(idx1)
        if n1 < n0:
            idx0, _ = train_test_split(
                idx0,
                test_size=1 - n1 / n0,
                random_state=random_state
            )
        idx = np.hstack((idx0, idx1))
        return x[idx, :], y[idx]
